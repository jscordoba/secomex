<?php
	require "conn.php"; 
?>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Aplicacion Desarrollada para Magnetron S.A.S, destianada al área de Comercio Exterior (COMEX); con el fin de dar seguimiento y divulgación de las importaciones y/o exportaciones.">
    <meta name="author" content="Jeison Stevens Córdoba Sánchez">
    
    <link rel="shortcut icon" href="../img/Container.ico">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <!-- Optional theme -->

    <script type="text/javascript" src="../bootstrap/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../bootstrap/fonts/flaticon.css">

    <title>SECOMEX</title>
</head>
<br>
<div class="col-md-5 col-md-offset-1 container">

	<?php
		if (isset($_REQUEST['numero_seguimiento'])) {
			$numero_seguimiento=$_REQUEST['numero_seguimiento'];
			if (isset($_REQUEST['tipo_seg'])){
				$tipo_seguimiento=$_REQUEST['tipo_seg'];
			}else{
				$tipo_seguimiento="";
			}
		}else{
			$numero_seguimiento="";
		}

		if ($tipo_seguimiento=="Importacion") {
			$seguimiento="<b class='text-success'>Importación</b>";
		}elseif($tipo_seguimiento=="Exportacion"){
			$seguimiento="<b class='text-success'>Exportación</b>";
		}else{
			$seguimiento="<b class='text-success'>".$_REQUEST['tipo_seg']."</b>";
		}
	?>

	<div class="col-md-12">
		<br>
		<?php 
		//echo "prueba: ".$_REQUEST['numero_seguimiento']." - ".$_REQUEST['tipo_seg'];
		?>
		<h2><small>Novedades de la <?php echo $seguimiento." ".$numero_seguimiento; ?> </small></h2>
		<br>
		<table class="table" style="font-size:12;">
			<tr class="">
				<?php
					if ($tipo_seguimiento==1) {
						echo "<th>OCT</th>";
					}
				?>
				<th width="50%">Observaciones</th><th>Fecha estimada en Planta</th><th>Fecha Registro de Novedad</th>
			</tr>
			<?php
			$ver=mysqli_query($conn,"SELECT * FROM novedad WHERE seguimiento_seguimiento_numero='$numero_seguimiento'");

			while ($novedad=mysqli_fetch_array($ver,MYSQLI_BOTH)) {
				echo "<tr>";

				if ($tipo_seguimiento==1) {
					echo "<td>".$novedad[1]."</td>";
				}
					echo "<td>".$novedad[3]."</td><td>".$novedad[2]."</td><td>".$novedad[4]."</td>
					</tr>";
			}
			?>
			
		</table>
		
		<?php 
		if (isset($_REQUEST["invitado"])) {
			if ($_REQUEST["invitado"]==1 and isset($_REQUEST["tipo_seg"])){
				//echo $_REQUEST["tipo_seg"];

				if (isset($_REQUEST["tipo_seg"])) {
					if ($_REQUEST["tipo_seg"]=="") {
						echo "<a href=\"seguimiento_ext.php\" class=\"btn btn-success btn-xs\"> Volver</a>";
					}else{
						echo "<a href=\"seguimiento_ext.php?tipo=".$tipo_seguimiento."\" class=\"btn btn-success btn-xs\"> Volver</a>";
					}
				}else{
					echo "<a href=\"seguimiento_ext.php\" class=\"btn btn-success btn-xs\"> Volver</a>";
				}
			} 
		}else{
			echo "<a href=\"management.php?pag=ver-seguimiento\" class=\"btn btn-success\"> Volver</a>";
		}
		?>
		
	</div>
	<br>
	
</div>