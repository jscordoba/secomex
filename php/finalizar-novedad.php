<?php
	require "conn.php";
	if (isset($_REQUEST['numero_seguimiento'])) {
		$numero_seguimiento=$_REQUEST['numero_seguimiento'];
		$tipo_seguimiento=$_REQUEST['tipo_seg'];
	}else{
		$numero_seguimiento="";
	}

	if ($tipo_seguimiento=="Importacion") {
			$seguimiento="<b class='text-success'>Importación</b>";
	}else{
		$seguimiento="<b class='text-success'>Exportación</b>";
	}
?>
<br><br><br>
<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 container form-horizontal"><!-- IMPORTACION -->
	<h2>Finalizar <?php echo $seguimiento; ?> #<?php echo $numero_seguimiento; ?></h2>
	<br>
	<form action="php/sql.php" method="POST">
		<div class="form-group">
			<label for="nro_contrato" class="control-label col-md-1">Observaciones</label>
			<div class="col-md-4">
				<textarea name="observacion_novedad" class="form-control" required>

				</textarea>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-2 col-md-offset-1">
				<?php
				echo "<input type=\"hidden\" name=\"tipo_seg\" value='".$tipo_seguimiento."'>";
				echo "<input type=\"hidden\" name=\"numero_seguimiento\" value='".$numero_seguimiento."'>";
				?>
				<input type="hidden" name="accion" value="finalizar_novedad">
				<button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-download-alt"></span> Finalizar</button>
				<a href="management.php?pag=ver-seguimiento" class="btn btn-success"> Volver</a>
			</div>
		</div>
	</form>
</div>