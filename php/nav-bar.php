<?php
  require "conn.php";
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Nav</span>
        <span class="icon-bar">Inicio</span>
        <span class="icon-bar">Salir</span>
      </button>
      <a class="navbar-brand" href="management.php">COMEX - Magnetron S.A.S</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><?= $_SESSION['id_sesion']; ?></a></li>
        <li><a href="management.php">Inicio</a></li>
        <li><a href="management.php?pag=salir">Salir</a></li>
      </ul>
    </div>
  </div>
</nav>