<?php require "conn.php"; ?>
<br>
<br>
<br>
<div class="col-md-10 col-md-offset-1 col-sm-12 container">

	<div class="col-md-12">
		<h2>Seguimientos Abiertos</h2>
		<br>
		<table class="table table-hover text-center">
			<tr class="">
				<th><a href="#" data-toggle="modal" data-target="#myModal">Número</a></th>
				<th>Cliente / Proveedor</th><th>Tipo</th><th>Modo</th><th>INCOTERM</th><th>Fecha Apertura</th><th>Usuario Apertura</th><th>Estado</th><th class="text-center">Acción</th>
			</tr>
			<?php

			$ver=mysqli_query($conn,"SELECT * FROM v_seguimiento_descrip ORDER BY seguimiento_estado,seguimiento_fechaap DESC");

			if ($ver) {
				while ($seguimiento=mysqli_fetch_array($ver,MYSQLI_BOTH)) {
					if ($seguimiento[4]=="Cerrado") {
						echo "
						<tr>
							<td>".$seguimiento[1]."</td><td>".$seguimiento[2]."</td><td>".$seguimiento[6]."</td><td>".$seguimiento[3]."</td><td>".$seguimiento[7]."</td><td>".$seguimiento[5]."</td><td>".$seguimiento[8]."</td><td class=\"bg-warning text-success\"><b>".$seguimiento[4]."</b></td><td class='text-left'><a href='management.php?pag=ver-novedad&numero_seguimiento=".$seguimiento[1]."&tipo_seg=".$seguimiento[6]."' class='btn btn-success btn-sm'>Ver Novedades</a></td>
						</tr>";
					}else{
						echo "
							<tr>
								<td>".$seguimiento[1]."</td><td>".$seguimiento[2]."</td><td>".$seguimiento[6]."</td><td>".$seguimiento[3]."</td><td>".$seguimiento[7]."</td><td>".$seguimiento[5]."</td><td>".$seguimiento[8]."</td><td class=\"bg-warning text-success\"><b>".$seguimiento[4]."</b></td><td class='text-left'><a href='management.php?pag=nueva-novedad&numero_seguimiento=".$seguimiento[1]."&tipo_seg=".$seguimiento[6]."' class='btn btn-success btn-sm'>Agregar Novedad</a> <a href='management.php?pag=ver-novedad&numero_seguimiento=".$seguimiento[1]."&tipo_seg=".$seguimiento[6]."' class='btn btn-success btn-sm'>Ver Novedades</a> <a href='management.php?pag=finalizar-novedad&numero_seguimiento=".$seguimiento[1]."&tipo_seg=".$seguimiento[6]."' class='btn btn-danger btn-sm'>Finalizar</a></td>
							</tr>
						";
					}
				}
			}
			
			?>
		</table>
		<a href="management.php" class="btn btn-success"> Volver</a>

		<!--<nav class="text-center">
			<ul class="pagination pagination-md">
				<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li><a href="">5</a></li>
				<li><a href=""><span aria-hidden="true">&raquo;</span></a></li>
			</ul>
		</nav>-->
	</div>
