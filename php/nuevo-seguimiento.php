<?php
	require "conn.php";
?>
<br><br><br>
<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 container form-horizontal"><!-- IMPORTACION -->
	<h2>Nuevo seguimiento</h2>
	<br>
	<form action="php/sql.php" method="POST">
		<div class="form-group">
			<label for="nro_contrato" class="control-label col-md-2">Cliente/Proveedor</label>
			<div class="col-md-4">
				<input type="text" name="cli_prov_seguimiento" class="form-control" placeholder="Nombre de Cliente o Proveedor" required>
			</div>
		</div>

		<div class="form-group">
			<label for="objeto" class="control-label col-md-2">Tipo</label>
			<div class="col-md-4">
				<select class="form-control" name="tipo_seguimiento" required>
					<option value="">Seleccione el Tipo de Seguimiento</option>
					<option value="1">Importación</option>
					<option value="2">Exportación</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="contratista" class="control-label col-md-2">Número</label>
			<div class="col-md-4">
				<input type="text" name="numero_seguimiento" class="form-control" placeholder="Número de Importación o Exportación" required>
			</div>
		</div>

		<div class="form-group">
			<label for="interventor" class="control-label col-md-2">INCOTERM</label>
			<div class="col-md-4">
				<select class="form-control" name="incoterm_seguimiento" required>
					<option value="">Seleccione...</option>
				<?php 
					$sql=mysqli_query($conn,"SELECT * FROM incoterm");
					while ($incoterm=mysqli_fetch_array($sql,MYSQL_BOTH)) {
						echo "<option value='".$incoterm[0]."'>".$incoterm[1]."</option>";
					}
				?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="fecha_ini" class="control-label col-md-2">Modo</label>
			<div class="col-md-4">
				<select class="form-control" name="modo_seguimiento" required>
					<option value="">Seleccione un Modo de Transporte</option>
					<option option="1">Aereo</option>
					<option option="2">Maritimo</option>
					<option option="3">Terrestre</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-2 col-md-offset-2">
				<input type="hidden" name="accion" value="nuevo_seguimiento">
				<button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-download-alt"></span> Guardar</button>
				<a href="management.php" class="btn btn-success"> Volver</a>
			</div>
		</div>
	</form>
</div>
<br><br><br>


<!-- INSERT INTO `secomex`.`seguimiento` (`id_seguimiento`, `seguimiento_clie_prov`, `seguimiento_numero`, `tipo_seguimiento_idtipo_seguimiento`, `seguimiento_modo`, `icoterm_idicoterm`) VALUES (NULL, 'Jeison', '1010', '1', 'Aereo', '5');  -->
