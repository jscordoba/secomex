<?php
session_start();
	//$_SESSION['id_sesion']='invitado';
	require "conn.php";

	if (isset($_REQUEST['tipo'])) {
		$tipo=$_REQUEST['tipo'];
	}elseif (!isset($_REQUEST['tipo']) || $_REQUEST['tipo']=="") {
		$tipo="";
		$sql_busca="1";
	}	
?>
<!-- ESTILOS Y FUENTES Y SCRIPTS COMUNES PARA EL APLICATIVO-->
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Aplicacion Desarrollada para Magnetron S.A.S, destianada al área de Comercio Exterior (COMEX); con el fin de dar seguimiento y divulgación de las importaciones y/o exportaciones.">
    <meta name="author" content="Jeison Stevens Córdoba Sánchez">
    
    <link rel="shortcut icon" href="../img/Container.ico">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <!-- Optional theme -->

    <script type="text/javascript" src="../bootstrap/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../bootstrap/fonts/flaticon.css">

    <title>SECOMEX</title>
</head>
<body>
<div class="col-md-5 container">
	<div class="col-md-12">
		<!--<h2><small>Seguimientos Abiertos</small></h2>-->
		<br>
		<div class="form-group">
			<form action="seguimiento_ext.php" method="POST">
				<label class="input-group">Buscar: </label>
				<input type="number" name="busca_numero" placeholder="Número de Imp/Exp" class="input-xs form-group">
				<input type="hidden" name="tipo" value="<?php echo $tipo; ?>">
				<button type"submit" class="btn btn-info btn-xs form-group">Buscar</button> 
				<?php 
					if ($tipo!="") {
						echo "<a href=\"seguimiento_ext.php?tipo=".$tipo."\" class=\"btn btn-info btn-xs form-group\">Refrescar</a>";
					}else{
						echo "<a href=\"seguimiento_ext.php\" class=\"btn btn-info btn-xs form-group\">Refrescar</a>";
					} 
				?>
			</form>

		</div>

		<table cellpadding="0" cellspacing="0" class="table table-hover text-center" style="font-size:12;">
			<tr class="">
				<th><a href="#" data-toggle="modal" data-target="#myModal">Número</a></th>
				<th>Cliente / Proveedor</th><th>Tipo</th><th>Modo</th><th>INCOTERM</th><th>Fecha Apertura</th><th>Estado</th>
			</tr>
			<?php
			if (isset($_REQUEST['busca_numero']) and $tipo!="") {
				if ($_REQUEST['busca_numero']!="" || $_REQUEST['busca_numero']!=null) {
					$sql_busca="seguimiento_numero LIKE('%".$_REQUEST['busca_numero']."%')";
				}
				else{
					$sql_busca="tipo_seguimiento_descripcion='".$tipo."' and 1";
				}
			}elseif (!isset($_REQUEST['busca_numero']) and $tipo!="") {
				$sql_busca="tipo_seguimiento_descripcion='".$tipo."' and 1";
			}elseif (isset($_REQUEST['busca_numero']) and $tipo=="") {
				if ($_REQUEST['busca_numero']=="") {
					$sql_busca="1";
				}else{
					$sql_busca="seguimiento_numero LIKE('%".$_REQUEST['busca_numero']."%')";
				}
			}elseif (!isset($_REQUEST['busca_numero']) and $tipo=="") {
				$sql_busca="1";
			}

			$sql="SELECT * FROM v_seguimiento_descrip WHERE $sql_busca ORDER BY seguimiento_estado,seguimiento_fechaap DESC";
			$ver=mysqli_query($conn,$sql);
			//echo "SQL: ".$sql;

			if ($ver){
				$result_num=mysqli_num_rows($ver);

				if (!$result_num) {
					echo "<tr><td class='text-danger' colspan='7'><b>Número no encontrado</b></td></tr> ";
				}else{

					while ($seguimiento=mysqli_fetch_array($ver,MYSQLI_BOTH)) {
						if ($tipo!="") {
							$tipo=$seguimiento[6];
						}
						
						if ($seguimiento[4]=="Cerrado") {
							echo "
							<small>
							<tr>
								<td>".$seguimiento[1]."</td><td>".$seguimiento[2]."</td><td>".$seguimiento[6]."</td><td>".$seguimiento[3]."</td><td>".$seguimiento[7]."</td><td>".$seguimiento[5]."</td><td class=\"bg-warning text-success\"><b>".$seguimiento[4]."</b></td></td><td class='text-left'><a href='../php/ver-novedad.php?numero_seguimiento=".$seguimiento[1]."&tipo_seg=".$tipo."&invitado=1' class='btn btn-danger btn-xs'>Ver Novedades</a></td>
							</tr>";
						}else{
							echo "
								<tr>
									<td>".$seguimiento[1]."</td><td>".$seguimiento[2]."</td><td>".$seguimiento[6]."</td><td>".$seguimiento[3]."</td><td>".$seguimiento[7]."</td><td>".$seguimiento[5]."</td><td class=\"bg-warning text-success\"><b>".$seguimiento[4]."</b></td></td><td class='text-left'><a href='../php/ver-novedad.php?numero_seguimiento=".$seguimiento[1]."&tipo_seg=".$tipo."&invitado=1' class='btn btn-info btn-xs'>Ver Novedades</a></td>
								</tr>
							</small>
							";
						}
					}
				}
			}
			?>
		</table>
	</div>
</div>

</body>
</html>