<div class="col-md-10 col-md-offset-1 text-center vcenter">
  <h1 class="">Seguimiento de Importaciones y Exportaciones</h1>

  <section class="container row center-block">
  	<div class="col-xs-12 col-sm-6 col-md-4"><a href="management.php?pag=nuevo-seguimiento"><img src="img/packages2.svg" class="img-responsive img-circle img-inicio center-block" id="nuevo-seguimiento">Nuevo Seguimiento</a></div>
  	<div class="col-xs-12 col-sm-6 col-md-4"><a href="management.php?pag=ver-seguimiento"><img src="img/cargo13.svg" class="img-responsive img-circle img-inicio center-block img-success" id="ver-seguimiento">Ver Seguimientos</a></div>
  	<div class="col-xs-12 col-sm-6 col-md-4"><a href="management.php?pag=salir"><img src="img/deliverytruck5.svg" class="img-responsive img-circle img-inicio center-block" id="logout">Salir</a></div>
  </section>
</div>