<?php
	require "conn.php";
	if (isset($_REQUEST['numero_seguimiento'])) {
		$numero_seguimiento=$_REQUEST['numero_seguimiento'];
		$tipo_seguimiento=$_REQUEST['tipo_seg'];
	}else{
		$numero_seguimiento="";
	}

	if ($tipo_seguimiento=="Importacion") {
			$seguimiento="<b class='text-success'>Importación</b>";
	}else{
		$seguimiento="<b class='text-success'>Exportación</b>";
	}
?>
<br><br><br>
<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 container form-horizontal"><!-- IMPORTACION -->
	<h2>Nueva novedad a <?php echo $seguimiento; ?> #<?php echo $numero_seguimiento; ?></h2>
	<br>
	<form action="php/sql.php" method="POST">
		
		<?php
		if ($tipo_seguimiento=='Importacion') {
			?>
			<div class="form-group">
				<label for="nro_contrato" class="control-label col-md-2">OCT</label>
				<div class="col-md-4">
					<input type="number" placeholder="Número de OCT" name="oct_novedad" class="form-control" required>
				</div>
			</div>

			<div class="form-group">
				<label for="contratista" class="control-label col-md-2">Fecha estimada en Planta</label>
				<div class="col-md-4">
					<input type="date" name="f_estimada_novedad1" class="form-control">
				</div>
			</div>
			<?php
		}else{
			?>
			<div class="form-group">
				<label for="contratista" class="control-label col-md-2">Fecha estimada de Entrega</label>
				<div class="col-md-4">
					<input type="date" name="f_estimada_novedad2" class="form-control" required>
				</div>
			</div>
		<?php
		}
		?>
		
		<div class="form-group">
			<label for="nro_contrato" class="control-label col-md-2">Observaciones</label>
			<div class="col-md-4">
				<textarea name="observacion_novedad" class="form-control" required>

				</textarea>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-2 col-md-offset-2">
				<input type="hidden" name="accion" value="nueva_novedad">
				<?php
				echo "<input type=\"hidden\" name=\"tipo_seg\" value='".$tipo_seguimiento."'>";
				echo "<input type=\"hidden\" name=\"numero_seguimiento\" value='".$numero_seguimiento."'>";
				?>
				<button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-download-alt"></span> Guardar</button>
				<a href="management.php?pag=ver-seguimiento" class="btn btn-success"> Volver</a>
			</div>
		</div>
	</form>
</div>
<br><br><br>