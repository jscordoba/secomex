
<!-- ESTILOS Y FUENTES Y SCRIPTS COMUNES PARA EL APLICATIVO-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Aplicacion Desarrollada para Magnetron S.A.S, destianada al área de Comercio Exterior (COMEX); con el fin de dar seguimiento y divulgación de las importaciones y/o exportaciones.">
    <meta name="author" content="Jeison Stevens Córdoba Sánchez">
    
    <link rel="shortcut icon" href="img/Container.ico">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <!-- Optional theme -->

    <script type="text/javascript" src="bootstrap/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/fonts/flaticon.css">

    <title>SECOMEX</title>

<?php
    require "adm_pag.php";//llama al administrador de paginas
?>
