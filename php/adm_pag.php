<?php

//--------------------- FUNCIONES PARA LLAMAR LOS COMPONENTES DEL SITIO------------------------
    function pag($pag)
    {
        switch ($pag) {
            case 'nav-bar':
                require "php/nav-bar.php";
                break;
            case 'footer':
                require "php/footer.php";
                break;
            case 'inicio':
                require "php/inicio.php";
                break;
            case 'nuevo-seguimiento':
                require "php/nuevo-seguimiento.php";
                break;
            case 'nueva-novedad':
                require "php/nueva-novedad.php";
                break;
            case 'ver-seguimiento':
                require "php/ver-seguimiento.php";
                break;
            case 'ver-novedad':
                require "php/ver-novedad.php";
                break;
            case 'finalizar-novedad':
                require "php/finalizar-novedad.php";
                break;
            case 'salir':
                require "logout.php";
                break;
            default:
                # code...
                break;
        }
    }

    
?>
<!--#############################--><!--#############################-->