<?php
	session_start();
	require "conn.php";

	session_unset();

	session_destroy();
	header("location: http://portal.magnetron.com/secomex/");
?>