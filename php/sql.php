<?php
session_start();

 require "conn.php";
 $hoy=date("Y-m-d");

 	if ($_REQUEST['accion']) {
 		$accion=$_REQUEST['accion'];

 		if ($accion=="nuevo_seguimiento") {
 			$cli_prov_seguimiento=$_POST['cli_prov_seguimiento'];
 			$numero_seguimiento=$_POST['numero_seguimiento'];
 			$tipo_seguimiento=$_POST['tipo_seguimiento'];
 			$incoterm_seguimiento=$_POST['incoterm_seguimiento'];
 			$modo_seguimiento=$_POST['modo_seguimiento'];

 			$sql_ins=mysqli_query($conn,"INSERT INTO seguimiento VALUES (null,'$cli_prov_seguimiento','$numero_seguimiento',$tipo_seguimiento,'$modo_seguimiento',$incoterm_seguimiento,".$_SESSION['id_user'].",'Abierto','$hoy')");

 			if ($sql_ins) {
 				header("location:../management.php?pag=ver-seguimiento");
 			}else{
 				echo "Error al insertar Seguimiento.";
 			}
 		}

 		if ($accion=="nueva_novedad") {
 			$numero_seguimiento=$_POST['numero_seguimiento'];
 			$observacion_novedad=$_POST['observacion_novedad'];

 			$sql_id=mysqli_query($conn,"SELECT id_seguimiento FROM seguimiento WHERE seguimiento_numero='".$numero_seguimiento."'");


 			if ($id_seguimiento=mysqli_fetch_array($sql_id,MYSQLI_BOTH)) {

 				//echo $id_seguimiento[0];

 				if (isset($_REQUEST['tipo_seg']) and $_REQUEST['tipo_seg']=="Importacion") {
	 				//importacion
	 				$oct_novedad=$_POST['oct_novedad'];
	 				$f_estimada_novedad1=$_POST['f_estimada_novedad1'];

	 				$sql_ins=mysqli_query($conn,"INSERT INTO novedad VALUES (null,'$oct_novedad','$f_estimada_novedad1','$observacion_novedad','$hoy',".$id_seguimiento[0].",1,'$numero_seguimiento')");

		 			if ($sql_ins) {
		 				header("location:../management.php?pag=ver-novedad&numero_seguimiento=".$numero_seguimiento."&tipo_seg=".$_REQUEST['tipo_seg']."");
		 			}else{
		 				echo "Error novedad1";
		 			}
	 			}else if ($_REQUEST['tipo_seg']=="Exportacion") {
	 				//exportacion
	 				$f_estimada_novedad2=$_POST['f_estimada_novedad2'];

	 				$query="INSERT INTO novedad VALUES (null,null,'$f_estimada_novedad2','$observacion_novedad','$hoy',".$id_seguimiento[0].",1,'$numero_seguimiento')";
	 				$sql_ins=mysqli_query($conn,$query);


		 			if ($sql_ins) {
		 				header("location:../management.php?pag=ver-novedad&numero_seguimiento=".$numero_seguimiento."&tipo_seg=".$_REQUEST['tipo_seg']."");
		 			}else{
		 				echo "Error novedad2".$query;
		 			}
	 			} 	
 			}
 		}

 		if ($accion=="finalizar_novedad") {
 			$numero_seguimiento=$_POST['numero_seguimiento']; 
 			$observacion_novedad=$_POST['observacion_novedad'];			
 			
			$sql_id=mysqli_query($conn,"SELECT id_seguimiento FROM seguimiento WHERE seguimiento_numero='".$numero_seguimiento."'");

			if ($id_seguimiento=mysqli_fetch_array($sql_id,MYSQLI_BOTH)) {
				$query="INSERT INTO novedad VALUES (null,null,null,'$observacion_novedad','$hoy',".$id_seguimiento[0].",".$_SESSION['id_user'].",'$numero_seguimiento')";
				$sql_ins=mysqli_query($conn,$query);
	 			//$sql_ins=mysqli_query($conn,"UPDATE seguimiento SET seguimiento_estado='Cerrado' WHERE numero_seguimiento=".$numero_seguimiento."");

	 			if ($sql_ins) {
	 				$sql_ins2=mysqli_query($conn,"UPDATE seguimiento SET seguimiento_estado='Cerrado' WHERE seguimiento_numero='".$numero_seguimiento."' and id_seguimiento=".$id_seguimiento[0]."");
	 				
	 				if ($sql_ins2) {
	 					header("location:../management.php?pag=ver-seguimiento");
	 				}else{
	 					echo "Error al Actualizar Seguimiento";
	 				}
	 			}else{
	 				echo "Error al Insertar Ultima Novedad";
	 			}
	 		}
 		}
 	}
?>