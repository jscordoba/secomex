<?php
  session_start();

  if (!isset($_SESSION['id_sesion']))
  {
    header("Location:index.html");
  }else{

    ?>
<!DOCTYPE html>
<html lang="en">
<?php
  require "php/styles.php"; //llamar hojas de estilos

?>

  <body>
    <?php
      pag('nav-bar'); //llamar barra navegacion superior
    ?>

    <div class="container-fluid"><!-- INICIO DEL CONTENEDOR GENERAL-->        
        <div class="col-xs-12 col-sm-12 col-md-12"><!-- CONTENEDOR PARA CONTENIDO PRINCIPAL -->
              <section class="row">
              <?php
                if (isset($_REQUEST['pag'])) {
                  $pag=$_REQUEST['pag'];
                }else{
                  $pag='inicio';
                }

                pag($pag);
              ?>
              </section>
              <br><br><br><br><br>
              <?php
                pag('footer'); //llamar funcion de footer
              ?>
              
        </div>
    </div><!-- FIN DE CONTENEDOR GENERAL -->

  </body>
</html>
<?php 
  }
?>