
<!-- -->
$(document).ready(function(){
      $(".menu_inicial").css("display","block");
      $(".form_nuevo").css("display","none");
      $("#nuevo_tipo_item").css("display","none");
      $("#nuevo_tipo_vehiculo").css("display","none");
      $("#nuevo_tipo_item_preparacion").css("display","none");



   //------------------EVENTOS MENU LATERAL------------------------------------------------
     $(".ir_preparar_despacho").click(function(event) {
      /* Act on the event */
      $(".menu_inicial").css("display","block");
      $(".menu_preparar_trafos").css("display","none");
      $(".menu_preparar_camion").css("display","none");
      $(".menu_iniciar_cargue").css("display","none");

      $(".menu_admin_trafos").css("display","none");
      $(".menu_reportes").css("display","none");
      $(".menu_usuarios").css("display","none");
      $(".menu_admin_vehiculos").css("display","none");
    });
     $(".ir_vehiculos").click(function(event) {
      /* Act on the event */
      $(".menu_admin_trafos").css("display","none");
      $(".menu_reportes").css("display","none");
      $(".menu_usuarios").css("display","none");
      $(".menu_inicial").css("display","none");
      
      $(".menu_preparar_trafos").css("display","none");
      $(".menu_iniciar_cargue").css("display","none");
      $(".menu_preparar_camion").css("display","none");
      $(".menu_admin_vehiculos").css("display","block");
      
      $(".lista").addClass("active");
      $(".nuevo").removeClass("active");
      $(".form_nuevo").css("display","none");
      $(".t_listados").css("display","block");
    });
     $(".ir_trafos").click(function(event) {
      /* Act on the event */
     $(".menu_admin_vehiculos").css("display","none");
      $(".menu_admin_trafos").css("display","block");
      $(".menu_reportes").css("display","none");
      $(".menu_usuarios").css("display","none");

      $(".menu_inicial").css("display","none");
      $(".menu_preparar_trafos").css("display","none");
      $(".menu_iniciar_cargue").css("display","none");
      $(".menu_preparar_camion").css("display","none");

      $(".lista").addClass("active");
      $(".nuevo").removeClass("active");
      $(".form_nuevo").css("display","none");
      $(".t_listados").css("display","block");
      $("#alto").val();
      $("#ancho").val();
      $("#largo").val();
    });
     $(".ir_reportes").click(function(event) {
      /* Act on the event */
      $(".menu_admin_vehiculos").css("display","none");
      $(".menu_admin_trafos").css("display","none");
      $(".menu_reportes").css("display","block");
      $(".menu_usuarios").css("display","none");

      $(".menu_inicial").css("display","none");
      $(".menu_preparar_trafos").css("display","none");
      $(".menu_iniciar_cargue").css("display","none");
      $(".menu_preparar_camion").css("display","none");
    });
     $(".ir_usuarios").click(function(event) {
      /* Act on the event */
      $(".menu_admin_vehiculos").css("display","none");
      $(".menu_admin_trafos").css("display","none");
      $(".menu_reportes").css("display","none");
      $(".menu_usuarios").css("display","block");

      $(".menu_inicial").css("display","none");
      $(".menu_preparar_trafos").css("display","none");
      $(".menu_iniciar_cargue").css("display","none");
      $(".menu_preparar_camion").css("display","none");

      $(".lista").addClass("active");
      $(".nuevo").removeClass("active");
      $(".form_nuevo").css("display","none");
      $(".t_listados").css("display","block");
      $("#alto").val();
      $("#ancho").val();
      $("#largo").val();
    });

    $(".mostrar_menu").click(function(){
        //$(".columnas").css("background-color", "red");
        $('.mostrar_menu').css("display","none");
        $('.text_lateral').show(500);
        //$('.ocultar_menu').show("fast");
    });

    $(".ocultar_menu").click(function(){
        $('.text_lateral').css("display","none");
        $('.mostrar_menu').show(500);
    });


  //------------------OPCIONES DEL MENU INICIAL PARA LA PREPARACION DE CARGUE------------------------------------------------
    $(".m_ini_preparar_camion").click(function(event) {
      /* Act on the event */
      $(".menu_inicial").css("display","none");
      $(".menu_preparar_trafos").css("display","none");
      $(".menu_iniciar_cargue").css("display","none");
      $(".menu_preparar_camion").css("display","block");

      $(".menu_admin_vehiculos").css("display","none");
      $(".menu_admin_trafos").css("display","none");
      $(".menu_reportes").css("display","none");
      $(".menu_usuarios").css("display","none");
    });

    $(".m_ini_preparar_trafos").click(function(event) {
      /* Act on the event */
      $(".menu_inicial").css("display","none");
      $(".menu_preparar_trafos").css("display","block");
      $(".menu_iniciar_cargue").css("display","none");
      $(".menu_preparar_camion").css("display","none");

      $(".menu_admin_vehiculos").css("display","none");
      $(".menu_admin_trafos").css("display","none");
      $(".menu_reportes").css("display","none");
      $(".menu_usuarios").css("display","none");

      $("#nuevo_tipo_item").css("display","none");
      $("#plantilla_items").css("display","none");


    });


    $(".m_ini_iniciar_cargue").click(function(event) {
      /* Act on the event */
      $(".menu_inicial").css("display","none");
      $(".menu_preparar_trafos").css("display","none");
      $(".menu_iniciar_cargue").css("display","block");
      $(".menu_preparar_camion").css("display","none");

      $(".menu_admin_vehiculos").css("display","none");
      $(".menu_admin_trafos").css("display","none");
      $(".menu_reportes").css("display","none");
      $(".menu_usuarios").css("display","none");
    });

    //------------------ACTIVIDAD DE LISTADOS Y NUEVOS------------------------------------------------

    $(".nuevo").click(function(event) {
      $(".lista").removeClass("active");
      $(".nuevo").addClass("active");
      $(".t_listados").css("display","none");
      $(".form_nuevo").css("display","block");
    });

    $(".lista").click(function(event) {
      $(".nuevo").removeClass("active");
      $(".lista").addClass("active");
      $(".form_nuevo").css("display","none");
      $(".t_listados").css("display","block");
    });




    $("#crear_tipo_item").click(function(){//MOSTRAR OPCION PARA AGREGAR NUEVO TIPO DE ITEM
      $("#plantilla_items").css("display","none");
      $("#l_plantilla_items").css("display","none");
      $("#crear_tipo_item").css("display","none");
      $("#nuevo_tipo_item").css("display","block");
    });


    $("#crear_tipo_vehiculo").click(function(){ //MOSTRAR OPCION PARA AGREGAR NUEVO TIPO DE VEHICULO
      $("#plantilla_vehiculos").css("display","none");
      $("#l_plantilla_vehiculos").css("display","none");
      $("#crear_tipo_vehiculo").css("display","none");
      $("#nuevo_tipo_vehiculo").css("display","block");
    }); 	



    $("#btn_agregar_item").click(function(){
      //$("#btn_agregar_item").addClass("clicked");
      agregar_item();
    })

});

//------------------ INICIO ACTIVIDAD EN AMINISTRACION DE ITEMS------------------------------------------------

function tipo_item(item){
  var tipo_item=item;
  //alert(tipo_item);

  $("#plantilla_items").css("display","block");
  $("#l_plantilla_items").css("display","block");
  $("#crear_tipo_item").css("display","block");
  $("#nuevo_tipo_item").css("display","none");
  $(".dimensiones_item").remove(); // LIMPIAR LA LISTA PARA EL CAMBIO

  $.post("php/query.php",{tipo_item:item,query:"consulta_dimensiones_tipo"},function(response){
    //alert(response);
    
    var items=response.split("|");
    for (var i = 0; i < items.length-1; i++) {
      //alert(items[i]);
      var items2=items[i].split(";");
      for (var x = 0; x < items2.length-1; x++) {
        //alert(items2[0]+"-"+items2[1]);
        var items3=items2[1].split(",");
        for (var z = 0; z < items2.length-1; z++) {
          //alert(items2[0]+"-"+items3[0]+"-"+items3[1]+"-"+items3[2]);
          $("#plantilla_items").append("<a href='#' class=\"list-group-item dimensiones_item\" onclick=\"(asignar_dimensiones_item("+items3[0]+","+items3[1]+","+items3[2]+"))\"><b>"+items2[0]+"</b> ("+items3[0]+" x "+items3[1]+" x "+items3[2]+")"+"</a>");
        }
      }
    };
  });
}

function asignar_dimensiones_vehiculo(alto,ancho,largo){
  //alert("item "+alto+ancho+largo);
  $("#alto_vehiculo").val(alto);
  $("#ancho_vehiculo").val(ancho);
  $("#largo_vehiculo").val(largo);
}

function asignar_dimensiones_item(alto,ancho,largo){
  //alert("vehiculo "+alto+ancho+largo);
  $("#alto_item").val(alto);
  $("#ancho_item").val(ancho);
  $("#largo_item").val(largo);
}

function registrar_item(){
  var tipo_item=$("#tipo_item").val();
  var item=$("#item").val();
  var descripcion=$("#descripcion").val();
  var alto=$("#alto_item").val();
  var ancho=$("#ancho_item").val();
  var largo=$("#largo_item").val();
  var p_neto=$("#p_neto").val();
  var p_bruto=$("#p_bruto").val();

  //alert(tipo_item+"-"+item+"-"+descripcion+"-"+alto+"-"+ancho+"-"+largo+"-"+p_neto+"-"+p_bruto);
  $.post("php/query.php",{query:"insertar_item",tipo_item:tipo_item,item:item,descripcion:descripcion,alto:alto,ancho:ancho,largo:largo,p_neto:p_neto,p_bruto:p_bruto},function(result){

    if (result=="ok") {
      alert("Item Ingresado correctamente.");
      $("#tipo_item").val(); // SE LIMPIAN LOS CAMPOS
      $("#item").val("");
      $("#descripcion").val("");
      $("#alto").val("");
      $("#ancho").val("");
      $("#largo").val("");
      $("#p_neto").val("");
      $("#p_bruto").val("");
      location.reload(); //SE RECARGA PAGINA PARA VER LOS CAMBIOS
    }else if(result=="error"){
      alert("Error al ingresar el Item, favor revise los datos.");
    }
  });
}

function crear_tipo_item(){
  $("#plantilla_items").css("display","block");
  $("#l_plantilla_items").css("display","block");
  $("#crear_tipo_item").css("display","block");
  $("#nuevo_tipo_item").css("display","none"); 

  var desc_nuevo_t_item=$("#desc_nuevo_t_item").val();

  $.post("php/query.php",{query:"nuevo_tipo_item",desc_nuevo_t_item:desc_nuevo_t_item},function(result){
    
    if (result=="ok") {
      alert("Tipo Ingresado correctamente.");
      $("#plantilla_items").css("display","block");
      $("#l_plantilla_items").css("display","block");
      $("#crear_tipo_item").css("display","block");
      $("#nuevo_tipo_item").css("display","none");
      location.reload();//SE RECARGA PAGINA PARA VER LOS CAMBIOS
    }else if(result=="error"){
      alert("Error al ingresar el Tipo, favor reportar TIC's.");
    }
  });
}
//------------------FIN DE ACTIVIDAD EN AMINISTRACION DE ITEMS------------------------------------------------




//----------------------------------------------------------------------------------------------------------------
//------------------ INICIO ACTIVIDAD EN AMINISTRACION DE VEHICULOS------------------------------------------------

function tipo_vehiculo(item){
  var tipo_vehiculo=item;
  //alert(tipo_vehiculo);

  $("#plantilla_vehiculos").css("display","block");
  $("#l_plantilla_vehiculos").css("display","block");
  $("#crear_tipo_vehiculo").css("display","block");
  $("#nuevo_tipo_vehiculo").css("display","none");
  $(".dimensiones_vehiculo").remove(); // LIMPIAR LA LISTA PARA EL CAMBIO

  $.post("php/query.php",{tipo_vehiculo:tipo_vehiculo,query:"consulta_dimensiones_tipo_vehiculo"},function(response){
    //alert(response);
    
    var items=response.split("|");
    for (var i = 0; i < items.length-1; i++) {
      //alert(items[i]);
      var items2=items[i].split(";");
      for (var x = 0; x < items2.length-1; x++) {
        //alert(items2[0]+"-"+items2[1]);
        var items3=items2[1].split(",");
        for (var z = 0; z < items2.length-1; z++) {
          //alert(items2[0]+"-"+items3[0]+"-"+items3[1]+"-"+items3[2]);
          $("#plantilla_vehiculos").append("<a href='#' class=\"list-group-item dimensiones_vehiculo\" onclick=\"(asignar_dimensiones_vehiculo("+items3[0]+","+items3[1]+","+items3[2]+"))\"><b>"+items2[0]+"</b> ("+items3[0]+" x "+items3[1]+" x "+items3[2]+")"+"</a>");
        }
      }
    };
  });
}

function registrar_vehiculo(){
  var tipo_vehiculo=$("#tipo_vehiculo").val();
  var placa=$("#placa").val();
  var n_troques=$("#n_troques").val();
  var peso_max=$("#peso_max").val();
  var ancho=$("#ancho").val();
  var alto=$("#alto").val();
  var largo=$("#largo").val();
  var transportador=$("#transportador").val();

  //alert(tipo_vehiculo+"-"+placa+"-"+n_troques+"-"+peso_max+"-"+ancho+"-"+alto+"-"+largo+"-"+transportador);
  $.post("php/query.php",{query:"insertar_vehiculo",tipo_vehiculo:tipo_vehiculo,placa:placa,n_troques:n_troques,peso_max:peso_max,ancho:ancho,alto:alto,largo:largo,transportador:transportador},function(result){

    if (result=="ok") {
      alert("Item Ingresado correctamente.");
      $("#tipo_vehiculo").val(); // SE LIMPIAN LOS CAMPOS
      $("#placa").val("");
      $("#n_troques").val("");
      $("#peso_max").val("");
      $("#ancho").val("");
      $("#alto").val("");
      $("#largo").val("");
      $("#transportador").val("");
      location.reload(); //SE RECARGA PAGINA PARA VER LOS CAMBIOS
    }else if(result=="error"){
      alert("Error al ingresar el Item, favor revise los datos.");
    }
  });
}

function crear_tipo_vehiculo(){
  $("#plantilla_vehiculos").css("display","block");
  $("#l_plantilla_vehiculos").css("display","block");
  $("#crear_tipo_vehiculo").css("display","block");
  $("#nuevo_tipo_vehiculo").css("display","none");

  var desc_nuevo_t_vehiculo=$("#desc_nuevo_t_vehiculo").val();

  //alert(desc_nuevo_t_vehiculo);

  $.post("php/query.php",{query:"nuevo_tipo_vehiculo",desc_nuevo_t_vehiculo:desc_nuevo_t_vehiculo},function(result){
    
    if (result=="ok") {
      alert("Tipo Ingresado correctamente.");
      $("#plantilla_vehiculos").css("display","block");
      $("#l_plantilla_vehiculos").css("display","block");
      $("#crear_tipo_vehiculo").css("display","block");
      $("#nuevo_tipo_vehiculo").css("display","none");
      location.reload();//SE RECARGA PAGINA PARA VER LOS CAMBIOS
    }else if(result=="error"){
      alert("Error al ingresar el Tipo, favor reportar TIC's.");
    }
  });
}
//------------------FIN DE ACTIVIDAD EN AMINISTRACION DE VEHICULOS------------------------------------------------



//----------------------------------------------------------------------------------------------------------------
//------------------ INICIO ACTIVIDAD PREPARACION DE VEHICULOS------------------------------------------------
function mostrar_vehiculos(vehiculo, opcion){
  $(".vehiculos").remove();
  
  if (opcion=="tipo") {
    $("#lista_vehiculos option").remove();

    $.post("php/query.php",{query:"mostrar_vehiculos_tipo",vehiculo:vehiculo},function(result){

      var items=result.split("|");
      for (var i = 0; i < items.length-1; i++) {
        //alert(items[i]);
        var items2=items[i].split(";");
        for (var x = 0; x < items2.length-1; x++) {

          var items3=items2[1].split(",");
          for (var z = 0; z < items2.length-1; z++) {
            //alert(items2[0]+"-"+items3[0]+"-"+items3[1]+"-"+items3[2]);
            //$("#lista_vehiculos").append("<option value=\""+items2[0]+"\"><b>"+items2[1]+"</b> ("+items2[2]+" x "+items2[3]+" x "+items2[4]+")</option>");
            $("#lista_vehiculos").append("<option value=\""+items2[0]+"\"><b>"+items3[0]+"</b> ("+items3[1]+" x "+items3[2]+" x "+items3[3]+")</option>");
          }
        }
      };

    });
  };

  var tipo_vehiculo=$("#buscar_tipo_vehiculo").val();

  //alert(tipo_vehiculo+"-"+vehiculo+"-"+opcion);

  $.post("php/query.php",{query:"mostrar_vehiculo",opcion:opcion,vehiculo:vehiculo,tipo_vehiculo:tipo_vehiculo},function(result){
    var items=result.split("|");
    for (var i = 0; i < items.length-1; i++) {
      //alert(items[i]);
      var items2=items[i].split(";");
      for (var x = 0; x < items2.length-1; x++) {
        //alert(items2[0]+"-"+items2[1]);
        var items3=items2[1].split(",");
        for (var z = 0; z < items2.length-1; z++) {
          //alert(items2[0]+"-"+items3[0]+"-"+items3[1]+"-"+items3[2]);
          $("#resultado_vehiculos").append("<div id='"+items2[0]+"' onclick=\"agregar_cargue('vehiculo', '"+items2[0]+"')\" class=\"col-sm-3 col-md-3 col-xs-3 vehiculos\"><img src=\"img/Camion.JPG\" alt=\"...\" class=\"img-circle img_menu_vehiculo\"><div class=\"caption\"><h3>"+items2[0]+"</h3><p>Vehiculo tipo <b>"+items3[0]+"</b> con un peso Maximo de <b>"+items3[1]+" Kgs</b>.</p></div></div>");
        }
      }
    };
  });  

}

//------------------FIN DE ACTIVIDAD PREPARACION DE VEHICULOS------------------------------------------------


//----------------------------------------------------------------------------------------------------------------
//------------------ INICIO ACTIVIDAD PREPARACION DE ITEMS------------------------------------------------

function tipo_item_preparacion(item){
  var tipo_item=item;
  //alert(tipo_item);

  $("#plantilla_items_preparacion a").remove();

  $("#plantilla_items_preparacion").css("display","block");
  $("#l_plantilla_items_preparacion").css("display","block");

  $.post("php/query.php",{tipo_item:item,query:"consulta_dimensiones_tipo"},function(response){
    //alert(response);
    
    var items=response.split("|");
    for (var i = 0; i < items.length-1; i++) {
      //alert(items[i]);
      var items2=items[i].split(";");
      for (var x = 0; x < items2.length-1; x++) {
        //alert(items2[0]+"-"+items2[1]);
        var items3=items2[1].split(",");
        for (var z = 0; z < items2.length-1; z++) {
          //alert(items2[0]+"-"+items3[0]+"-"+items3[1]+"-"+items3[2]);
          $("#plantilla_items_preparacion").append("<a href='#' onclick=\"agregar_cargue('item', "+items2[0]+")\" class=\"list-group-item items_plantilla\" id='"+items2[0]+"'><b>"+items2[0]+"</b> ("+items3[0]+" x "+items3[1]+" x "+items3[2]+")"+"</a>");
        }
      }
    };
  });
}

var items_ids = [];//VARIABLE GLOBAL ITEMS SELECCIONADOS PARA CARGUE

function agregar_cargue(tipo, id_tipo){ // AGREGAR SELECCION DE VEHICULO O ITEM A LA TABLA TEMPORAL
  var num_item;
  alert(tipo+"-"+id_tipo);

  if (tipo=="vehiculo") {
    var placa=id_tipo;

    if ($("#"+id_tipo).hasClass('active')) { //se valida si ya fue seleccionada un vehiculo en especifico
      alert("Ya fue seleccionado");
    }else{
      $("#"+id_tipo).addClass("active");

      $.post("php/query.php",{query:"ingresar_vehiculo_despacho",placa:placa},function(result){

        alert(result);

        if (result=="actualizado") {
          alert("Se ha Actualizado el Vehiculo en Disposición de Cargue.");
        }else if(result=="nuevo"){
          alert("Se ha agregado el Vehiculo para Disposición de Cargue.");
        }else if(result=="error_actualizar"){
          alert("Error al ingresar Actualizado.");
        }else if (result=="error_nuevo") {
          alert("Error al ingresar Nuevo.");
        };
      });
    }
  }else if (tipo=="item") {
    if ($("#"+id_tipo).hasClass('active')) {//se valida si ya fue seleccionada un item especifico
      $("#"+id_tipo).removeClass("active");
      items_ids[num_item]=null;
    }else{
      $("#"+id_tipo).addClass("active");
      num_item=items_ids.length;
      alert(num_item);
      items_ids[num_item]=id_tipo;
    }
  };
}

function agregar_item(){//AGREGAR EL ITEM SELECCIONADO CON CANTIDAD A LA TABLA
  var tipo_item_preparacion=$("#tipo_item_preparacion").val();
  var cantidad=$("#cantidad_item").val();

  if (cantidad=="" || cantidad==0 || cantidad=="0" || cantidad==null) {
    alert("Debe digitar una cantidad");
  }else if (!$(".items_plantilla").hasClass('active')) {
    alert("Debe Seleccionar 1 item");
  }else{
    for (var i =0; i < items_ids.length; i++) {
      alert(items_ids[i]);
    };
    alert("Vals: "+items_ids+" - "+tipo_item_preparacion+" - "+cantidad);

    $.post("php/query.php",{query:"ingresar_item_despacho",tipo_item_preparacion:tipo_item_preparacion,items_ids:items_ids,cantidad:cantidad},function(result2){

      alert(result2);

      if (result2=="OK") {
        //alert(result);

        $(".t_resumen_trafos_cargue").append("<tr><td>1</td><td>1244fdgd3</td><td>5-1-Vp.7620 Vs. 240</td><td>3 Und</td><td>3 mts</td><td>2 mts</td><td>2 mts</td><td>50 Kg</td><td>86 Kg</td><td>Sin despachar</td><td> - </td></tr>");
        $(".items_plantilla").removeClass("active");
        $("#cantidad_item").val("");
      }else{
        alert("No se pudo agregar el Item, favor revise.");
      }
    });
  }
}

//------------------FIN DE ACTIVIDAD PREPARACION DE ITEMS------------------------------------------------